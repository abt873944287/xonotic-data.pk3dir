#ifndef DEBUGPATHING
	#define DEBUGPATHING 0
#endif

#include "costs.qc"
#include "expandnode.qc"
#include "main.qc"
#include "movenode.qc"
#include "path_waypoint.qc"
#include "utility.qc"
#if DEBUGPATHING
	#include "debug.qc"
#endif
