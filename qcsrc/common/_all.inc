float autocvar_net_connecttimeout = 30;

#ifndef MENUQC
#include "anim.qc"
#include "animdecide.qc"
#include "ent_cs.qc"
#include "net_notice.qc"
#endif

#include "mapinfo.qc"
#include "playerstats.qc"
#ifdef SVQC
    #include "state.qc"
#endif
#include "util.qc"

#ifndef CSQC
#include "campaign_file.qc"
#include "campaign_setup.qc"
#endif

#ifndef MENUQC
#include "physics/all.inc"
#include "triggers/include.qc"
#include "viewloc.qc"
#endif

#ifndef MENUQC
#include "minigames/minigames.qc"
#endif

#include "debug.qh"

#ifndef MENUQC
#include "deathtypes/all.qc"
#include "effects/all.qc"
#include "impulses/all.qc"
#include "notifications/all.qc"
#include "t_items.qc"
#endif

#include "items/_mod.inc"
    #include "weapons/all.qc"
        #include "monsters/all.qc"
        #include "turrets/all.qc"
        #include "vehicles/all.qc"

#include "mutators/_mod.inc"
    #include "gamemodes/_mod.inc"
