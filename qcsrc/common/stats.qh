#ifndef STATS_H
#define STATS_H

#ifdef SVQC
#include <server/cl_client.qh>
#endif

// Full list of all stat constants, included in a single location for easy reference
// 255 is the current limit (MAX_CL_STATS - 1), engine will need to be modified if you wish to add more stats

const int MAX_CL_STATS = 256;

// const int STAT_HEALTH = 0;
// const int STAT_ARMOR = 4;
// const int STAT_SHELLS = 6;
// const int STAT_NAILS = 7;
// const int STAT_ROCKETS = 8;
// const int STAT_CELLS = 9;
// const int STAT_ACTIVEWEAPON = 10;
// const int STAT_ITEMS = 15; // .items | .items2 << 23 | serverflags << 28
// const int STAT_VIEWHEIGHT = 16;

#if defined(CSQC)
    #define g_stat_HEALTH getstati(STAT_HEALTH)
    #define g_stat_ARMOR getstat_int(STAT_ARMOR)
    #define g_stat_SHELLS getstat_int(STAT_SHELLS)
    #define g_stat_NAILS getstat_int(STAT_NAILS)
    #define g_stat_ROCKETS getstat_int(STAT_ROCKETS)
    #define g_stat_CELLS getstat_int(STAT_CELLS)
    #define g_stat_ACTIVEWEAPON getstat_int(STAT_ACTIVEWEAPON)
    #define g_stat_ITEMS getstat_int(STAT_ITEMS)
    #define g_stat_VIEWHEIGHT getstati(STAT_VIEWHEIGHT)
#elif defined(SVQC)
    #define stat_HEALTH health
    #define stat_ARMOR armorvalue
    #define stat_SHELLS ammo_shells
    #define stat_NAILS ammo_nails
    #define stat_ROCKETS ammo_rockets
    #define stat_CELLS ammo_cells
    #define stat_ACTIVEWEAPON weapon
    #define stat_ITEMS items
    #define stat_VIEWHEIGHT view_ofs_z
#endif

REGISTER_STAT(WEAPONS, vectori)
REGISTER_STAT(WEAPONSINMAP, vectori)

REGISTER_STAT(PL_VIEW_OFS, vector, autocvar_sv_player_viewoffset)
REGISTER_STAT(PL_CROUCH_VIEW_OFS, vector, autocvar_sv_player_crouch_viewoffset)

REGISTER_STAT(PL_MIN, vector, autocvar_sv_player_mins)
REGISTER_STAT(PL_CROUCH_MIN, vector, autocvar_sv_player_crouch_mins)

REGISTER_STAT(PL_MAX, vector, autocvar_sv_player_maxs)
REGISTER_STAT(PL_CROUCH_MAX, vector, autocvar_sv_player_crouch_maxs)

REGISTER_STAT(KH_KEYS, int)

/** weapon requested to switch to; next WANTED weapon (for HUD) */
REGISTER_STAT(SWITCHWEAPON, int)
/** weapon currently being switched to (is copied from switchweapon once switch is possible) */
REGISTER_STAT(SWITCHINGWEAPON, int)
REGISTER_STAT(WEAPON_NEXTTHINK, float)
#ifdef SVQC
SPECTATE_COPYFIELD(_STAT(WEAPON_NEXTTHINK))
float W_WeaponRateFactor(entity this);
#endif
REGISTER_STAT(WEAPONRATEFACTOR, float, W_WeaponRateFactor(this))

REGISTER_STAT(GAMESTARTTIME, float)
REGISTER_STAT(STRENGTH_FINISHED, float)
REGISTER_STAT(INVINCIBLE_FINISHED, float)
/** arc heat in [0,1] */
REGISTER_STAT(ARC_HEAT, float)
REGISTER_STAT(PRESSED_KEYS, int)
/** this stat could later contain some other bits of info, like, more server-side particle config */
REGISTER_STAT(ALLOW_OLDVORTEXBEAM, bool)
REGISTER_STAT(FUEL, int)
REGISTER_STAT(NB_METERSTART, float)
/** compressShotOrigin */
REGISTER_STAT(SHOTORG, int)
REGISTER_STAT(LEADLIMIT, float)
REGISTER_STAT(WEAPON_CLIPLOAD, int)
REGISTER_STAT(WEAPON_CLIPSIZE, int)

REGISTER_STAT(VORTEX_CHARGE, float)
REGISTER_STAT(LAST_PICKUP, float)
REGISTER_STAT(HUD, int)
REGISTER_STAT(VORTEX_CHARGEPOOL, float)
REGISTER_STAT(HIT_TIME, float)
REGISTER_STAT(DAMAGE_DEALT_TOTAL, int)
REGISTER_STAT(TYPEHIT_TIME, float)
REGISTER_STAT(LAYED_MINES, int)
REGISTER_STAT(HAGAR_LOAD, int)
REGISTER_STAT(SUPERWEAPONS_FINISHED, float)
REGISTER_STAT(VEHICLESTAT_HEALTH, int)
REGISTER_STAT(VEHICLESTAT_SHIELD, int)
REGISTER_STAT(VEHICLESTAT_ENERGY, int)
REGISTER_STAT(VEHICLESTAT_AMMO1, int)
REGISTER_STAT(VEHICLESTAT_RELOAD1, int)
REGISTER_STAT(VEHICLESTAT_AMMO2, int)
REGISTER_STAT(VEHICLESTAT_RELOAD2, int)
REGISTER_STAT(VEHICLESTAT_W2MODE, int)
REGISTER_STAT(NADE_TIMER, float)
REGISTER_STAT(SECRETS_TOTAL, float)
REGISTER_STAT(SECRETS_FOUND, float)
REGISTER_STAT(RESPAWN_TIME, float)
REGISTER_STAT(ROUNDSTARTTIME, float)
REGISTER_STAT(MONSTERS_TOTAL, int)
REGISTER_STAT(MONSTERS_KILLED, int)
REGISTER_STAT(BUFFS, int)
REGISTER_STAT(NADE_BONUS, float)
REGISTER_STAT(NADE_BONUS_TYPE, int)
REGISTER_STAT(NADE_BONUS_SCORE, float)
REGISTER_STAT(HEALING_ORB, float)
REGISTER_STAT(HEALING_ORB_ALPHA, float)
REGISTER_STAT(PLASMA, int)
REGISTER_STAT(OK_AMMO_CHARGE, float)
REGISTER_STAT(OK_AMMO_CHARGEPOOL, float)
REGISTER_STAT(FROZEN, int)
REGISTER_STAT(REVIVE_PROGRESS, float)
REGISTER_STAT(ROUNDLOST, int)
REGISTER_STAT(BUFF_TIME, float)
REGISTER_STAT(CTF_FLAGSTATUS, int)
REGISTER_STAT(ENTRAP_ORB, float)
REGISTER_STAT(ENTRAP_ORB_ALPHA, float)

#ifdef SVQC
int autocvar_g_multijump;
float autocvar_g_multijump_add;
float autocvar_g_multijump_speed;
float autocvar_g_multijump_maxspeed;
float autocvar_g_multijump_dodging = 1;
#endif
REGISTER_STAT(MULTIJUMP_DODGING, int, autocvar_g_multijump_dodging)
REGISTER_STAT(MULTIJUMP_MAXSPEED, float, autocvar_g_multijump_maxspeed)
REGISTER_STAT(MULTIJUMP_ADD, int, autocvar_g_multijump_add)
REGISTER_STAT(MULTIJUMP_SPEED, float, autocvar_g_multijump_speed)
.int multijump_count;
REGISTER_STAT(MULTIJUMP_COUNT, int, this.multijump_count)
REGISTER_STAT(MULTIJUMP, int, autocvar_g_multijump)
REGISTER_STAT(DOUBLEJUMP, int, autocvar_sv_doublejump)

#ifdef SVQC
bool g_bugrigs;
bool g_bugrigs_planar_movement;
bool g_bugrigs_planar_movement_car_jumping;
float g_bugrigs_reverse_spinning;
float g_bugrigs_reverse_speeding;
float g_bugrigs_reverse_stopping;
float g_bugrigs_air_steering;
float g_bugrigs_angle_smoothing;
float g_bugrigs_friction_floor;
float g_bugrigs_friction_brake;
float g_bugrigs_friction_air;
float g_bugrigs_accel;
float g_bugrigs_speed_ref;
float g_bugrigs_speed_pow;
float g_bugrigs_steer;
#endif
REGISTER_STAT(BUGRIGS, int, g_bugrigs)
REGISTER_STAT(BUGRIGS_ACCEL, float, g_bugrigs_accel)
REGISTER_STAT(BUGRIGS_AIR_STEERING, int, g_bugrigs_air_steering)
REGISTER_STAT(BUGRIGS_ANGLE_SMOOTHING, int, g_bugrigs_angle_smoothing)
REGISTER_STAT(BUGRIGS_CAR_JUMPING, int, g_bugrigs_planar_movement_car_jumping)
REGISTER_STAT(BUGRIGS_FRICTION_AIR, float, g_bugrigs_friction_air)
REGISTER_STAT(BUGRIGS_FRICTION_BRAKE, float, g_bugrigs_friction_brake)
REGISTER_STAT(BUGRIGS_FRICTION_FLOOR, float, g_bugrigs_friction_floor)
REGISTER_STAT(BUGRIGS_PLANAR_MOVEMENT, int, g_bugrigs_planar_movement)
REGISTER_STAT(BUGRIGS_REVERSE_SPEEDING, int, g_bugrigs_reverse_speeding)
REGISTER_STAT(BUGRIGS_REVERSE_SPINNING, int, g_bugrigs_reverse_spinning)
REGISTER_STAT(BUGRIGS_REVERSE_STOPPING, int, g_bugrigs_reverse_stopping)
REGISTER_STAT(BUGRIGS_SPEED_POW, float, g_bugrigs_speed_pow)
REGISTER_STAT(BUGRIGS_SPEED_REF, float, g_bugrigs_speed_ref)
REGISTER_STAT(BUGRIGS_STEER, float, g_bugrigs_steer)

REGISTER_STAT(GAMEPLAYFIX_DOWNTRACEONGROUND, int, cvar("sv_gameplayfix_downtracesupportsongroundflag"))
REGISTER_STAT(GAMEPLAYFIX_EASIERWATERJUMP, int, cvar("sv_gameplayfix_easierwaterjump"))
REGISTER_STAT(GAMEPLAYFIX_STEPDOWN, int, cvar("sv_gameplayfix_stepdown"))
REGISTER_STAT(GAMEPLAYFIX_STEPMULTIPLETIMES, int, cvar("sv_gameplayfix_stepmultipletimes"))
REGISTER_STAT(GAMEPLAYFIX_UNSTICKPLAYERS, int, cvar("sv_gameplayfix_unstickplayers"))
REGISTER_STAT(GAMEPLAYFIX_UPVELOCITYCLEARSONGROUND, int, autocvar_sv_gameplayfix_upwardvelocityclearsongroundflag)

REGISTER_STAT(MOVEVARS_JUMPSTEP, int, cvar("sv_jumpstep"))
REGISTER_STAT(NOSTEP, int, cvar("sv_nostep"))

REGISTER_STAT(MOVEVARS_FRICTION, float)
REGISTER_STAT(MOVEVARS_FRICTION_SLICK, float, autocvar_sv_friction_slick)
REGISTER_STAT(MOVEVARS_FRICTION_ONLAND, float, autocvar_sv_friction_on_land)

REGISTER_STAT(MOVEVARS_JUMPSPEEDCAP_DISABLE_ONRAMPS, int, autocvar_sv_jumpspeedcap_max_disable_on_ramps)
REGISTER_STAT(MOVEVARS_TRACK_CANJUMP, int)
/** cvar loopback */
REGISTER_STAT(MOVEVARS_CL_TRACK_CANJUMP, int)

#ifdef SVQC
int g_dodging;
float autocvar_sv_dodging_delay;
float autocvar_sv_dodging_wall_distance_threshold;
bool autocvar_sv_dodging_frozen;
bool autocvar_sv_dodging_frozen_doubletap;
float autocvar_sv_dodging_height_threshold;
float autocvar_sv_dodging_horiz_speed;
float autocvar_sv_dodging_horiz_speed_frozen;
float autocvar_sv_dodging_ramp_time;
float autocvar_sv_dodging_up_speed;
bool autocvar_sv_dodging_wall_dodging;
#endif

REGISTER_STAT(DODGING, int, g_dodging)
REGISTER_STAT(DODGING_DELAY, float, autocvar_sv_dodging_delay)
REGISTER_STAT(DODGING_DISTANCE_THRESHOLD, float, autocvar_sv_dodging_wall_distance_threshold)
REGISTER_STAT(DODGING_FROZEN, int, autocvar_sv_dodging_frozen)
REGISTER_STAT(DODGING_FROZEN_NO_DOUBLETAP, int, autocvar_sv_dodging_frozen_doubletap)
REGISTER_STAT(DODGING_HEIGHT_THRESHOLD, float, autocvar_sv_dodging_height_threshold)
REGISTER_STAT(DODGING_HORIZ_SPEED, float, autocvar_sv_dodging_horiz_speed)
REGISTER_STAT(DODGING_HORIZ_SPEED_FROZEN, float, autocvar_sv_dodging_horiz_speed_frozen)
REGISTER_STAT(DODGING_RAMP_TIME, float, autocvar_sv_dodging_ramp_time)
/** cvar loopback */
REGISTER_STAT(DODGING_TIMEOUT, float)
REGISTER_STAT(DODGING_UP_SPEED, float, autocvar_sv_dodging_up_speed)
REGISTER_STAT(DODGING_WALL, int, autocvar_sv_dodging_wall_dodging)

REGISTER_STAT(JETPACK_ACCEL_SIDE, float, autocvar_g_jetpack_acceleration_side)
REGISTER_STAT(JETPACK_ACCEL_UP, float, autocvar_g_jetpack_acceleration_up)
REGISTER_STAT(JETPACK_ANTIGRAVITY, float, autocvar_g_jetpack_antigravity)
REGISTER_STAT(JETPACK_FUEL, float, autocvar_g_jetpack_fuel)
REGISTER_STAT(JETPACK_MAXSPEED_SIDE, float, autocvar_g_jetpack_maxspeed_side)
REGISTER_STAT(JETPACK_MAXSPEED_UP, float, autocvar_g_jetpack_maxspeed_up)

REGISTER_STAT(MOVEVARS_HIGHSPEED, float, autocvar_g_movement_highspeed)

// freeze tag, clan arena
REGISTER_STAT(REDALIVE, int)
REGISTER_STAT(BLUEALIVE, int)
REGISTER_STAT(YELLOWALIVE, int)
REGISTER_STAT(PINKALIVE, int)

// domination
REGISTER_STAT(DOM_TOTAL_PPS, float)
REGISTER_STAT(DOM_PPS_RED, float)
REGISTER_STAT(DOM_PPS_BLUE, float)
REGISTER_STAT(DOM_PPS_YELLOW, float)
REGISTER_STAT(DOM_PPS_PINK, float)

REGISTER_STAT(TELEPORT_MAXSPEED, float, autocvar_g_teleport_maxspeed)
REGISTER_STAT(TELEPORT_TELEFRAG_AVOID, int, autocvar_g_telefrags_avoid)

REGISTER_STAT(SPECTATORSPEED, float)

#ifdef SVQC
#include "physics/movetypes/movetypes.qh"
#endif

REGISTER_STAT(MOVEVARS_AIRACCEL_QW_STRETCHFACTOR, float)
REGISTER_STAT(MOVEVARS_AIRCONTROL_PENALTY, float)
REGISTER_STAT(MOVEVARS_AIRSPEEDLIMIT_NONQW, float)
REGISTER_STAT(MOVEVARS_AIRSTRAFEACCEL_QW, float)
REGISTER_STAT(MOVEVARS_AIRCONTROL_POWER, float)
noref bool autocvar_sv_gameplayfix_nogravityonground;
REGISTER_STAT(MOVEFLAGS, int, MOVEFLAG_VALID
                              | (autocvar_sv_gameplayfix_q2airaccelerate ? MOVEFLAG_Q2AIRACCELERATE : 0)
                              | (autocvar_sv_gameplayfix_nogravityonground ? MOVEFLAG_NOGRAVITYONGROUND : 0)
                              | (autocvar_sv_gameplayfix_gravityunaffectedbyticrate ? MOVEFLAG_GRAVITYUNAFFECTEDBYTICRATE : 0))

REGISTER_STAT(MOVEVARS_WARSOWBUNNY_AIRFORWARDACCEL, float)
REGISTER_STAT(MOVEVARS_WARSOWBUNNY_ACCEL, float)
REGISTER_STAT(MOVEVARS_WARSOWBUNNY_TOPSPEED, float)
REGISTER_STAT(MOVEVARS_WARSOWBUNNY_TURNACCEL, float)
REGISTER_STAT(MOVEVARS_WARSOWBUNNY_BACKTOSIDERATIO, float)

REGISTER_STAT(MOVEVARS_AIRSTOPACCELERATE, float)
REGISTER_STAT(MOVEVARS_AIRSTRAFEACCELERATE, float)
REGISTER_STAT(MOVEVARS_MAXAIRSTRAFESPEED, float)
REGISTER_STAT(MOVEVARS_AIRCONTROL, float)
REGISTER_STAT(FRAGLIMIT, float, autocvar_fraglimit)
REGISTER_STAT(TIMELIMIT, float, autocvar_timelimit)
REGISTER_STAT(WARMUP_TIMELIMIT, float, warmup_limit)
#ifdef SVQC
float autocvar_sv_wallfriction;
#endif
REGISTER_STAT(MOVEVARS_WALLFRICTION, int, autocvar_sv_wallfriction)
REGISTER_STAT(MOVEVARS_TICRATE, float, autocvar_sys_ticrate)
REGISTER_STAT(MOVEVARS_TIMESCALE, float, autocvar_slowmo)
REGISTER_STAT(MOVEVARS_GRAVITY, float, autocvar_sv_gravity)
REGISTER_STAT(MOVEVARS_STOPSPEED, float)
REGISTER_STAT(MOVEVARS_MAXSPEED, float)
REGISTER_STAT(MOVEVARS_ACCELERATE, float)
REGISTER_STAT(MOVEVARS_AIRACCELERATE, float)
.float gravity;
// FIXME: Was 0 on server, 1 on client. Still want that?
REGISTER_STAT(MOVEVARS_ENTGRAVITY, float, (this.gravity) ? this.gravity : 1)
REGISTER_STAT(MOVEVARS_JUMPVELOCITY, float)
REGISTER_STAT(MOVEVARS_MAXAIRSPEED, float)
REGISTER_STAT(MOVEVARS_STEPHEIGHT, float, autocvar_sv_stepheight)
REGISTER_STAT(MOVEVARS_AIRACCEL_QW, float)
REGISTER_STAT(MOVEVARS_AIRACCEL_SIDEWAYS_FRICTION, float)


#ifdef CSQC
noref int autocvar_cl_gunalign;
#endif
#ifdef SVQC
.int cvar_cl_gunalign;
REPLICATE(cvar_cl_gunalign, int, "cl_gunalign");
#endif
REGISTER_STAT(GUNALIGN, int, this.cvar_cl_gunalign)
#ifdef SVQC
SPECTATE_COPYFIELD(_STAT(GUNALIGN))
#endif


#endif
